# procfs-maps

The goal of this program is to list removed libraries used in running processes, in case of updates. It works on Linux and uses `/proc/{pid}/maps` files.

Note: This program has been replaced by a [Python script in my dotfiles](https://gitlab.com/Exagone313/dotfiles/blob/master/shell/pythonscript/procfs-maps).
