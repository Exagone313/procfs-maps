NAME ?= procfs-maps

SRC := \
	crc32.c \
	list_destroy.c \
	list_init.c \
	list_set.c \
	main.c

CFLAGS ?= -O2
CPPFLAGS ?= -Wall -Wextra -Werror
LDLIBS ?=
CPPFLAGS += -MMD -MP -Isrc

OBJDIR ?= obj
SRCDIR := src
OBJ_PREFIX := $(OBJDIR)/$(SRCDIR)/
OBJ := $(addprefix $(OBJ_PREFIX),$(SRC:.c=.o))

.PHONY: all
all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(OBJ) $(LDLIBS)

$(OBJDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $(filter %.c,$^)

-include $(OBJ:.o=.d)

.PHONY: clean
clean:
	$(RM) $(OBJ) $(TEST_CANONICAL_PATH_OBJ)
	@$(RM) $(OBJ:.o=.d) $(TEST_CANONICAL_PATH_OBJ:.o=.d)
	@rmdir -p $(sort $(dir $(OBJ))) 2>/dev/null || true

.PHONY: fclean
fclean: clean
	$(RM) $(NAME) test/$(TEST_CANONICAL_PATH_NAME)

.PHONY: re
re:
	$(MAKE) fclean
	$(MAKE) all
