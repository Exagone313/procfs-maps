/*
 * Copyright (C) 2019 Elouan Martinet <exa@elou.world>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#define _DEFAULT_SOURCE

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "list.h"

#define PATHBUF_SIZE (sizeof("/proc//maps") + 256)
#define READ_SIZE 4096

static char *prefix = NULL;
static size_t prefix_len = 0;
static int deleted = 0;

static int is_numeric(char *filename)
{
	while (*filename)
		if (!isdigit(*filename++))
			return 0;
	return 1;
}

/*
 * A line is assumed to be lesser than READ_SIZE.
 */

// 7fcdb23fc000-7fcdb27fd000 rw-s 00000000 00:1a 325971                     /dev/shm/.org.chromium.Chromium.EEEGsv (deleted)

static t_list *read_maps(int fd)
{
	char buf[READ_SIZE * 2];
	size_t i, l, pathstart, lineend;
	int r;
	t_list *list;

	i = 0;
	l = 0;
	if (NULL == (list = list_init(48)))
		return NULL;
	while ((r = read(fd, buf + i, READ_SIZE)) > 0)
	{
		l += (size_t)r;
		pathstart = 0;
		while (pathstart < l && buf[pathstart] != '/')
			pathstart++;
		if (pathstart == l) // multiple lines without a path?
		{
			i = 0;
			l = 0;
			continue;
		}
		lineend = pathstart + 1;
		while (lineend < l && buf[lineend] != '\n')
			lineend++;
		if (lineend == l) // path ends out of buffer
		{
			l -= pathstart;
			memmove(buf, buf + pathstart, l);
			i = l + 1;
			continue;
		}
		if ((prefix_len == 0
					|| (lineend - pathstart > prefix_len
						&& memcmp(buf + pathstart, prefix, prefix_len) == 0))
				&& (!deleted
					|| (lineend - pathstart > sizeof(" (deleted)") - 1
						&& memcmp(buf + lineend - (sizeof(" (deleted)") - 1),
							" (deleted)", sizeof(" (deleted)") - 1) == 0)))
		{
			buf[lineend] = '\0';
			list_set(list, buf + pathstart, lineend - pathstart + 1);
		}
		i = 0;
		l = 0;
	}
	if (r < 0)
	{
		list_destroy(list);
		return NULL;
	}
	return list;
}

static void read_process(char *pidstr)
{
	char pathbuf[PATHBUF_SIZE];
	char exepath[PATH_MAX + 1];
	int fd;
	t_list *maps;
	ssize_t r;

	snprintf(pathbuf, PATHBUF_SIZE, "/proc/%s/exe", pidstr);
	if ((r = readlink(pathbuf, exepath, PATH_MAX)) < 0)
	{
		perror(pathbuf);
		goto clean_errno;
	}
	exepath[r] = '\0';

	snprintf(pathbuf, PATHBUF_SIZE, "/proc/%s/maps", pidstr);
	if ((fd = open(pathbuf, O_RDONLY)) < 0)
	{
		perror(pathbuf);
		goto clean_errno;
	}
	if (NULL == (maps = read_maps(fd)))
	{
		perror("read");
		goto clean_close;
	}
	for (size_t i = 0; i < maps->size; i++)
	{
		if (maps->elements[i].data)
			printf("%s:%s:%s\n", pidstr, exepath, (char *)maps->elements[i].data);
	}
	fflush(stdout);
	list_destroy(maps);

clean_close:
	close(fd);
clean_errno:
	errno = 0;
}

int main(int argc, char **argv)
{
	DIR *procfs;
	struct dirent *dirp;

	if (argc > 1)
	{
		prefix = argv[1];
		prefix_len = strlen(prefix);
	}
	if (argc > 2)
		deleted = 1;
	if (NULL == (procfs = opendir("/proc")))
	{
		perror("/proc (procfs)");
		return 1;
	}
	errno = 0;
	while (NULL != (dirp = readdir(procfs)))
	{
		if (dirp->d_type == DT_DIR && is_numeric(dirp->d_name))
			read_process(dirp->d_name);
	}
	if (errno)
		perror("readdir");
	closedir(procfs);
}
